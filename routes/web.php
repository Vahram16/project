<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index')->name('index');
Route::post('/add-book-to-basket', 'BasketController@addBookToBasket'); // add-to-basket @AddBookToBasket
Route::get('/basket', 'BasketController@index')->name('basketIndex');
Route::post('/add-book-count-in-basket', 'BasketController@addBookCountInBasket')->name('addBookCountInBasket');
Route::post('/minus-book-count-in-basket', 'BasketController@minusBookCountInBasket')->name('minusBookCountInBasket');
Route::post('/change-book-count-in-basket', 'BasketController@changeBookCountInBasket')->name('successOrder');
Route::post('/delete-book-from-basket', 'BasketController@deleteBookFromBasket')->name('deleteBookFromBasket');
Route::get('/login', 'LoginController@index')->name('home');
Route::post('/login', 'LoginController@postLogin')->name('postLogin');
Route::get('/logout', 'LoginController@logout')->name('logout');
Route::get('/order-process', 'ClientOrderController@orderProcess')->name('orderProcess');
Route::get('/create-client-order', 'ClientOrderController@createClientOrder')->name('createClientOrder');
Route::post('/store-order', 'ClientOrderController@storeClientOrder')->name('storeClientOrder');
Route::get('/success-order', 'ClientOrderController@successOrder')->name('successOrder');
Route::get('/create-contact', 'ActiveCampaignController@createContact')->name('createContact');
Route::post('/store-contact', 'ActiveCampaignController@storeContact')->name('storeContact');


Route::prefix('dashboard')->middleware('auth.check')->group(function () {

    Route::get('/', function () {
        return view('dashboard');
    });

    Route::prefix('user')->group(function () {

        Route::get('/', 'UserController@index')->name('indexUser');
        Route::get('/create', 'UserController@create')->name('createUser');
        Route::post('/store', 'UserController@store')->name('storeUser');
        Route::get('/edit/{id}', 'UserController@edit')->name('editUser');
        Route::post('/update/{id}', 'UserController@update')->name('updateUser');
        Route::get('delete/{id}', 'UserController@destroy')->name('deleteUser');
        Route::post('/search', 'UserController@search')->name('searchUser');

    });

    Route::prefix('product')->group(function () {
        Route::get('/', 'ProductController@index')->name('indexProduct');
        Route::get('/create', 'ProductController@create')->name('createProduct');
        Route::post('/store', 'ProductController@store')->name('storeProduct');
        Route::get('/edit/{id}', 'ProductController@edit')->name('editProduct');
        Route::post('/update/{id}', 'ProductController@update')->name('updateProduct');
        Route::get('delete/{id}', 'ProductController@destroy')->name('deleteProduct');
        Route::post('/search', 'ProductController@search')->name('searchProduct');
        Route::post('/destroy-image', 'ProductController@destroyImage')->name('destroyImage');
    });
    Route::prefix('order')->group(function () {
        Route::get('/', 'OrderController@index')->name('indexOrder');
        Route::get('/create', 'OrderController@create')->name('createOrder');
        Route::post('/store', 'OrderController@store')->name('storeOrder');
        Route::get('/edit/{id}', 'OrderController@edit')->name('editOrder');
        Route::post('/update/{id}', 'OrderController@update')->name('updateOrder');
        Route::get('delete/{id}', 'OrderController@destroy')->name('deleteOrder');
//        Route::post('/search', 'OrderController@search')->name('searchOrder');
        Route::post('/search', 'OrderController@search')->name('searchOrder');
        Route::post('/bookDestroy', 'OrderController@bookDestroy')->name('bookDestroy');
        Route::post('/changeStatus', 'OrderController@changeStatus');

    });


});
Route::get('/test', 'TestController@test');


