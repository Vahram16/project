<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::post('/login', 'Api\AuthController@login');
Route::get('/logout', 'Api\AuthController@logout')->middleware('auth.user');
Route::post('/register', 'Api\AuthController@register');
Route::get('/books', 'Api\BookController@index');
Route::post('/book-rating', 'Api\BookController@createBookRating')->middleware('auth.user');
Route::post('/create-order', 'Api\OrderController@createOrder')->middleware('auth.user');
Route::get('/ordered-book', 'Api\OrderController@getUserOrdersWithBooks')->middleware('auth.user');

Route::get('/test', 'TestController@test');
Route::get('/test-test', 'TestController@testTest');