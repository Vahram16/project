$(document).ready(function(){

   $('.destroy').on('click', function(){

        var id = $(this).data('id');
        var bookContainer = $(this).closest(".book");

        $.ajax({

            type: "POST",
            url: '/dashboard/order/bookDestroy',
            data: {
                id: id,
                _token: $('meta[name="csrf-token"]').attr('content')
            },
            success: function (response) {
                 if (response.success){
                     bookContainer.remove();
                 }
            }
        });

    });
});