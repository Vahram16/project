$(document).ready(function () {

    $('.orderStatus').on('change', function () {

        var value = $(this).val();
        var orderId = $(this).data('id');


        $.ajax({
            type: "POST",
            url: '/dashboard/order/changeStatus',
            data: {
                value: value,
                id:orderId,
                _token: $('meta[name="csrf-token"]').attr('content')
            },
            success: function (response) {
            }
        });

     });
});