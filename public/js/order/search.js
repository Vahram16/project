$(document).ready(function () {

    $('#txtSearch').on('keyup', function () {

        var value = $(this).val();

        $.ajax({

            type: "POST",
            url: '/dashboard/order/search',
            data: {
                value: value,
                _token: $('meta[name="csrf-token"]').attr('content')
            },
            success: function (response) {
                var resultHtml = '';

                $.each(response['orders'], function (key, value) {
                    var trExample = $('.tr_example')[0].outerHTML;

                    trExample = trExample.replace(new RegExp(':id', 'g'), value['id']);
                    trExample = trExample.replace(new RegExp(':name', 'g'), value['name']);
                    trExample = trExample.replace(new RegExp(':email', 'g'), value['email']);
                    trExample = trExample.replace(new RegExp(':customer_address', 'g'), value['customer_address']);
                    trExample = trExample.replace(new RegExp(':customer_phone', 'g'), value['customer_phone']);
                    trExample = trExample.replace(new RegExp('class="tr_example"', 'g'), '');

                    resultHtml = resultHtml + trExample;

                });

                $("#table tbody").html(resultHtml);

                $.each(response['orders'], function (key, value) {
                    $.each(value['books'], function (k, book) {
                        $("#myModal_" + value['id'] + " .modal-body").append(
                            '<span style="color: #0000ee">'+book['name']+':'+book['price']+'$'+'</span>'
                        );
                    });
                });
            }
        });
    });

});