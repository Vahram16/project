$(document).ready(function(){


    $('#txtSearch').on('keyup', function(){

        var text = $('#txtSearch').val();
x
        $.ajax({

            type:"POST",
            url: '/dashboard/product/search',
            data: {
                text: text,
                _token: $('meta[name="csrf-token"]').attr('content')
            },
            success: function(response) {
                var resultHtml = '';

                $.each(response['books'],function (key ,value) {

                    var trExample = $('.tr_example')[0].outerHTML;

                    trExample = trExample.replace(new RegExp(':id', 'g') , value['id']);
                    trExample = trExample.replace(new RegExp(':name', 'g') , value['name']);
                    trExample = trExample.replace(new RegExp(':author_name', 'g') , value['author_name']);
                    trExample = trExample.replace(new RegExp(':description', 'g') , value['description']);
                     if(value['images'].length) {
                         trExample = trExample.replace(new RegExp(':image', 'g'), '<img src="/images/products/'+value['images'][0]['image_name']+'" width="100" height="100">');
                         // trExample = trExample.replace(new RegExp(':image', 'g'), value['images'][0]['image_name']);
                     }else{
                         trExample = trExample.replace(new RegExp(':image', 'g'), "No Image");
                     }
                    trExample = trExample.replace(new RegExp(':price', 'g') , value['price']);
                    trExample = trExample.replace(new RegExp('class="tr_example"', 'g') , '');

                    resultHtml = resultHtml + trExample;
                });

                $("#table tbody").html(resultHtml);

            }



        });


    });

});