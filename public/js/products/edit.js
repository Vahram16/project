$(document).ready(function () {


    var title = $('.description').data('book-description');
    CKEDITOR.instances.description.setData(title);

    $('.destroy').on('click', function () {

        var id = $(this).data('id');
        var imageContainer = $(this).closest(".div-images");

        $.ajax({

            type: "POST",
            url: '/dashboard/product/destroy-image',
            data: {
                id: id,
                _token: $('meta[name="csrf-token"]').attr('content')
            },
            success: function (response) {
                if (response.success) {
                    imageContainer.remove();
                }
            }
        });

    });
});