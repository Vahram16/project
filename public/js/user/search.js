$(document).ready(function () {

    $('#txtSearch').on('keyup', function () {

        var text = $(this).val();


        $.ajax({

            type: "POST",
            url: '/dashboard/user/search',
            data: {
                text: text,
                _token: $('meta[name="csrf-token"]').attr('content')
            },
            success: function (response) {
                var resultHtml = '';

                $.each(response['users'], function (key, value) {
                    console.log(value);


                    var trExample = $('.tr_example')[0].outerHTML;


                    trExample = trExample.replace(new RegExp(':id', 'g'), value['id']);
                    trExample = trExample.replace(new RegExp(':name', 'g'), value['name']);
                    trExample = trExample.replace(new RegExp(':email', 'g'), value['email']);
                    trExample = trExample.replace(new RegExp('class="tr_example"', 'g'), '');


                     resultHtml = resultHtml + trExample;



                });
                $("#table tbody").html(resultHtml);
                console.log(resultHtml);





            }


        });


    });

});