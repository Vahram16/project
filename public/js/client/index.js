$(document).ready(function () {
    var totalBasketCountContainer = $('.sum');
    var totalBasketPriceContainer = $('#totalPrice');

    $('.add-book-to-basket').on('click', function () {

        var bookId = $(this).data('id');

        $.ajax({
            type: "POST",
            url: '/add-book-to-basket',
            data: {
                id: bookId,
                _token: $('meta[name="csrf-token"]').attr('content')
            },
            success: function (response) {
                if (response.success == true) {
                    totalBasketCountContainer.text(response.basketBooksCount);
                } else {
                    alert("Something has gone wrong!!!")
                }
            }
        });
    });

    $('.add-book-count-in-basket').on('click', function () {

        var THIS = $(this);

        THIS.attr('disabled', true);


        var bookId = THIS.data('id');
        THIS.closest('tr').find('.minus-book-count-in-basket').attr('disabled', false);

        var bookPrice = THIS.data('price');
        console.log(bookPrice);

        $.ajax({
            type: "POST",
            url: '/add-book-count-in-basket',
            data: {
                id: bookId,
                _token: $('meta[name="csrf-token"]').attr('content')
            },
            success: function (response) {
                if (response.success) {
                    var updatedCount = response.updatedBookCountInBasket;
                    var updatedTotalPrice = totalBasketPriceContainer.data('price') + bookPrice;

                    THIS.closest('tr').find('.book-count').val(updatedCount);
                    THIS.closest('tr').find('.book-price').text(updatedCount * bookPrice);

                    totalBasketPriceContainer.text(updatedTotalPrice);
                    totalBasketPriceContainer.data('price', updatedTotalPrice);

                    THIS.attr('disabled', false);
                } else {
                    alert("Something has gone wrong!!!")
                }
            }
        });
    });

    $('.minus-book-count-in-basket').on('click', function () {

        var THIS = $(this);
        THIS.attr('disabled', true);
        var bookId = THIS.data('id');
        var bookPrice = THIS.data('price');
        // var bookId = $(this).data('id');
        // $('#book_' + bookId).find('.minusBook').attr('disabled', true);
        // var bookPrice = $(this).data('price');
        // var total = $('#totalPrice');
        // var booksPrice = $(this).data('price');
        // var totalBooksCount = $('#totalPrice').text();
        // var num = totalBooksCount.replace(/\s/g, '');
        // var number = parseInt(num);
        // var result = number - booksPrice;
        // total.text(result);


        $.ajax({

            type: "POST",
            url: '/minus-book-count-in-basket',
            data: {
                id: bookId,
                _token: $('meta[name="csrf-token"]').attr('content')
            },
            success: function (response) {
                if (response.success) {
                    var updatedCount = response.updatedBookCountInBasket;
                    var updatedTotalPrice = totalBasketPriceContainer.data('price') - bookPrice;

                    THIS.closest('tr').find('.book-count').val(updatedCount);
                    THIS.closest('tr').find('.book-price').text(updatedCount * bookPrice);

                    totalBasketPriceContainer.text(updatedTotalPrice);
                    totalBasketPriceContainer.data('price', updatedTotalPrice);
                    THIS.attr('disabled', false);

                    if (updatedCount == 1) {
                        THIS.attr('disabled', true);

                    }

                }
            }
        });

    });
    $('td').on('change', function () {
        THIS = $(this).closest('tr').find('.book-count');
        var updatedBookCount = THIS.val();

        var bookId = THIS.data('id');
        if (updatedBookCount <= 1) {
            updatedBookCount = 1;
            THIS.val(1)

        }
        if (updatedBookCount > 1) {
            $(this).closest('tr').find('.minus-book-count-in-basket').attr('disabled', false);
        }
        console.log(updatedBookCount);
        $.ajax({
            type: "POST",
            url: '/change-book-count-in-basket',
            data: {
                id: bookId,
                newBookCount: updatedBookCount,
                _token: $('meta[name="csrf-token"]').attr('content')
            },
            success: function (response) {
                if (response.success == true) {

                    var bookPrice = response.bookNewPrice;
                    THIS.closest('tr').find('.book-price').text(bookPrice);
                    var bookLastPrice = response.lastCount * response.bookPrice;
                    var bookNewPrice = bookPrice - bookLastPrice;
                    var totalBooksPrice = totalBasketPriceContainer.data('price') + bookNewPrice;
                    totalBasketPriceContainer.text(totalBooksPrice);
                    totalBasketPriceContainer.data('price', totalBooksPrice);

                } else {
                    alert("Something has gone wrong!!!")
                }
            }
        });


    })
    $('.delete-book-from-basket').on('click', function () {

        var THIS = $(this);
        var bookId = THIS.data('id');
        var book = THIS.closest('#book_' + bookId);
        var bookPrice = THIS.data('price');


        $.ajax({

            type: "POST",
            url: '/delete-book-from-basket',
            data: {
                id: bookId,
                _token: $('meta[name="csrf-token"]').attr('content')
            },
            success: function (response) {
                if (response.success) {

                    var totalBookPrice = response.count * bookPrice;
                    var updatedTotalPrice = totalBasketPriceContainer.data('price') - totalBookPrice;
                    book.remove();
                    totalBasketPriceContainer.text(updatedTotalPrice);
                    totalBasketPriceContainer.data('price', updatedTotalPrice);
                }


            }
        });

    });


});