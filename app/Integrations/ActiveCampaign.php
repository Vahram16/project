<?php

namespace App\Integrations;

use App\Traits\ApiClient;
use Illuminate\Support\Facades\Validator;

/**
 * Class ActiveCampaign
 * @package App\Integrations
 */
class ActiveCampaign
{
    use ApiClient;

    private $apiKey;
    private $apiUrl = "https://avetisyanvahram.api-us1.com/api/3/";
    private $contactId;

    /**
     * ActiveCampaign constructor.
     * @param $apiKey
     */
    public function __construct($apiKey)
    {
        $this->apiKey = $apiKey;

    }

    public function createContact(array $contact)
    {
        $validator = Validator::make($contact, [
            'contact.email' => 'required|email',
            'contact.firstName' => 'string',
            'contact.lastName' => 'string',
            'contact.phone' => 'string',
        ]);

        if ($validator->fails()) {
            return $validator->errors()->getMessages();
        }

        $request = $this->post($this->apiUrl . "contacts", ['Api-Token' => $this->apiKey], $contact);
        return $request;
    }

    public function retrieveContact($contactId)
    {

        $request = $this->get($this->apiUrl . 'contacts/' . $contactId, ['Api-Token' => $this->apiKey]);
        return $request;

    }

    public function updateContact($contactId, array $contact)
    {
        $validator = Validator::make($contact, [
            'contact.email' => 'email',
            'contact.firstName' => 'string',
            'contact.lastName' => 'string',
            'contact.phone' => 'integer',
        ]);
        if ($validator->fails()) {
            return $validator->errors()->getMessages();
        }
        $request = $this->put($this->apiUrl . 'contacts/' . $contactId, ['Api-Token' => $this->apiKey], $contact);
        return $request;

    }

}


?>