<?php

namespace App\Integrations;

use Http\Adapter\Guzzle6\Client as GuzzleAdapter;
use Illuminate\Support\Facades\Validator;
use GuzzleHttp\Client;
use SparkPost\SparkPost as Spark;

class SparkPost
{
    private $apiKey;
    private $sparky;
    public $errors;

    public function __construct($apiKey)
    {
        $this->apiKey = $apiKey;
        $httpClient = new GuzzleAdapter(new Client());
        $this->sparky = new Spark($httpClient, ['key' => $apiKey]);
        $this->sparky->setOptions(['async' => false]);

    }

    /**
     * @return mixed
     */
    public function sendEmail(array $emailArray)
    {
        $validator = Validator::make($emailArray, [
            'email' => 'required|email',
            'subject' => 'required',
            'content' => 'required',
            'to' => 'required|email'
        ]);

        if ($validator->fails()) {
            return $validator->errors()->getMessages();
        }

        $results = $this->sparky->transmissions->post([
            'options' => [
                'sandbox' => true
            ],
            'content' => [
                'from' => $emailArray['email'],
                'subject' => $emailArray['subject'],
                'html' => '<html><body><p>' . $emailArray['content'] . '</p></body></html>'
            ],
            'recipients' => [
                ['address' => $emailArray['to']]
            ]
        ]);
    }
}
