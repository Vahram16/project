<?php

namespace App\Listeners;

use App\Events\ChangeOrderStatusEvent;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class ChangeOrderStatusListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  ChangeOrderStatusEvent  $event
     * @return void
     */
    public function handle(ChangeOrderStatusEvent $event)
    {
      $order = $event->getOrder();
      $order->update([
          'status' => 1
      ]);
    }
}
