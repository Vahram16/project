<?php

namespace App\Traits;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;

trait ApiClient
{
    public function post($url, array $headers, array $body)
    {
        $client = new Client(['headers' => $headers]);
        $response = [];

        try {
            $result = $client->post($url, ['json' => $body]);
        } catch (ClientException $e) {
            $response['success'] = false;
            $response['data'] = $e->getMessage();
        }

        $result = json_decode($result->getBody()->getContents(), true);

        $response['success'] = true;
        $response['data'] = $result;

        return $response;
    }

    public function get($url, array $headers)
    {
        $client = new Client(['headers' => $headers]);
        $response = [];


        try {
            $result = $client->get($url);
        } catch (ClientException $e) {
            $response['success'] = false;
            $response['data'] = $e->getMessage();
        }
        $result = json_decode($result->getBody()->getContents(), true);

        $response['success'] = true;
        $response['data'] = $result;
        return $response;

    }

    public function put($url, array $headers, array $body)
    {

        $client = new Client(['headers' => $headers]);
        $response = [];


        try {
            $result = $client->put($url, ['json' => $body]);
        } catch (ClientException $e) {
            $response['success'] = false;
            $response['data'] = $e->getMessage();
        }
        $result = json_decode($result->getBody()->getContents(), true);
        $response['success'] = true;
        $response['data'] = $result;
        return $response;

    }
}