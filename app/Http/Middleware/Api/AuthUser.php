<?php

namespace App\Http\Middleware\Api;

use App\UserToken;
use Closure;

class AuthUser
{

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        // 1. Check if Authorization header exists
        if (!$request->hasHeader('Authorization')){
            return response()->json([
                'message' => 'Authorization header is missing'
            ]);
        }

        $token = $request->bearerToken();

        if(empty($token)){
            return response()->json([
                'message' => 'Bearer token is empty'
            ]);
        }

        $userToken = UserToken::where('api_token',$token)->first();

        if (empty($userToken)) {
            return response()->json([
                'message' => 'Bearer token is invalid'
                ], 401);
        }

        $request->user = $userToken->user;

        return $next($request);
    }
}
