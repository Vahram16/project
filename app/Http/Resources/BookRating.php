<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class BookRating extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'rating' => $this->rating,
            'average_rating' => $this->book->average_rating,
            'comment' => $this->comment,
            'user_name' => $this->user->name
        ];
    }
}
