<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Book extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        $bookLast3Ratings = $this->bookLast3Ratings;

        $reviews = [];

        foreach ($bookLast3Ratings as $bookLast3Rating) {
            array_push($reviews, [
                'rating' => $bookLast3Rating->rating,
                'comment' => $bookLast3Rating->comment,
                'user_name' => $bookLast3Rating->user->name,
            ]);
        }

        $feUrl = env('FE_APP_URL');

        return [
            'id'             => $this->id,
            'name'           => $this->name,
            'price'          => $this->price,
            'image'          => count($this->images) ? "{$feUrl}/images/products/{$this->images[0]->image_name}" : "{$feUrl}/images/products/no_image.jpg",
            'average_rating' => $this->average_rating,
            'reviews'        => $reviews
        ];
    }
}
