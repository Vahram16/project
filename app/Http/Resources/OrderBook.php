<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class OrderBook extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        $orderBooks = $this->orderBooks;
        $orderedBooks = [];
        foreach ($orderBooks as $book) {
            array_push($orderedBooks, [
                'book' => $book->book ? $book->book->name : '',
                'count' => $book->count
            ]);
        }
        return [
            'order_id' => $this->id,
            'order_books' => $orderedBooks
        ];
    }
}
