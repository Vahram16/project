<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Order extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        $orderBooks = [];

            foreach ($this->orderBooks as $orderBook) {
                array_push($orderBooks, [
                    'book_id' => $orderBook->book_id,
                    'count' => $orderBook->count,
                    'price' => $orderBook->book->price
                ]);
        }

        return [
            'customer_address' => $this->customer_address,
            'customer_phone' => $this->customer_phone,
            'total_price' => $this->price,
            'ordered_books' => $orderBooks
        ];


    }
}
