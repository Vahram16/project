<?php

namespace App\Http\Controllers\Api;

use App\Book;
use App\Http\Resources\Order as OrderResource;
use App\Http\Resources\OrderBook as OrderBookResource;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use App\OrderBook;
use App\Order;
use App\Events\ChangeOrderStatusEvent;

class OrderController extends Controller
{

    public function createOrder(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'customer_phone' => 'required',
            'customer_address' => 'required',
            'order_books.*.book_id' => 'required|integer|exists:books,id',
            'order_books.*.quantity' => 'required|integer'
        ]);

        if ($validator->fails()) {
            $errors = $validator->errors();
            return response()->json([
                'success' => false,
                'errors' => $errors,
            ], 400);
        }

        $userId = $request->user->id;

        $orderBooks = [];
        $orderBookIds = [];

        foreach ($request->order_books as $orderBook) {
            array_push($orderBookIds, $orderBook['book_id']);
            $orderBooks[$orderBook['book_id']] = [
                'quantity' => $orderBook['quantity']
            ];
        }

        $books = Book::find($orderBookIds);

        $orderBooksTotalAmount = 0;

        foreach ($books as $book) {
            $orderBooksTotalAmount = $book->price * $orderBooks[$book->id]['quantity'] + $orderBooksTotalAmount;
        }

        $order = Order::create([
            'customer_id' => $userId,
            'customer_phone' => $request->customer_phone,
            'customer_address' => $request->customer_address,
            'price' => $orderBooksTotalAmount,
            'status' => 1
        ]);

        foreach ($books as $book) {

            OrderBook::create([
                'book_id' => $book->id,
                'order_id' => $order->id,
                'count' => $orderBooks[$book->id]['quantity']
            ]);

            event(new ChangeOrderStatusEvent($order));
        }

        $payload = new OrderResource($order);

        return response()->json([
            'success' => true,
            'payload' => $payload
        ], 200);
    }

    public function getUserOrdersWithBooks(Request $request)
    {
        $user = $request->user;

        if (count($user->orders)) {
            $orders = $user->orders;

            $payload = OrderBookResource::collection($orders);

            return response()->json([
                'success' => true,
                'payload' => $payload
            ], 200);
        }


        return response()->json([
            'success' =>false,
            'payload' => 'The user has no orders'
        ], 400);
    }
}
