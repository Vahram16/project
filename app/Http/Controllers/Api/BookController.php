<?php

namespace App\Http\Controllers\Api;

use App\Book;
use App\BookRating;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use App\Http\Resources\Book as BookResource;
use Illuminate\Support\Facades\Validator;
use App\Http\Resources\BookRating as BookRatingResource;

class BookController extends Controller
{
    public function index(Request $request)
    {
        $books = Book::paginate(2);
        $payload = [];

//        foreach ($books as $book){
//            array_push($payload , new BookResource($book));
//        }

        $payload = BookResource::collection($books);

        return response()->json([
            'success' => true,
            'payload' => $payload
        ]);
    }

    public function createBookRating(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'book_id' => 'required|integer',
            'rating' => 'required|integer',
            'comment' => 'required',
        ]);

        if ($validator->fails()) {
            $errors = $validator->errors();
            return response()->json([
                'success' => false,
                'errors' => $errors
            ], 400);
        }

        $userId = $request->user->id;
        $bookId = $request->book_id;

        $checkIfUserMadeReviewInLast24Hours = BookRating::where(['user_id' => $userId, 'book_id' => $bookId])
            ->where('created_at', '>', Carbon::now()->subHours(24))
            ->first();

        if ($checkIfUserMadeReviewInLast24Hours) {
            $createdTime = $checkIfUserMadeReviewInLast24Hours->created_at;
            $currentTime = Carbon::now()->diffInHours($createdTime);
            $restTime = 24 - $currentTime;
            return response()->json([
                'success' => false,
                'errors' => 'You can leave a review in '. $restTime .' hour'
            ]);
        }

        $bookRating = BookRating::create([
            'user_id' => $userId,
            'book_id' => $bookId,
            'rating' => $request->rating,
            'comment' => $request->comment
        ]);

        $book = Book::find($bookId);

        $bookRatings = $book->bookRatings;
        $totalBookRatingCount = count($bookRatings);
        $totalBookRating = 0;

        foreach ($bookRatings as $bookRating) {
            $totalBookRating = $bookRating->rating + $totalBookRating;
        }

        $averageRating = $totalBookRating / $totalBookRatingCount;
        $averageRating = round($averageRating, 1);

        $book->update([
            'average_rating' => $averageRating
        ]);

        $payload = new BookRatingResource($bookRating);

        return response()->json([
            'success' => true,
            'payload' => $payload
        ]);
    }
}
