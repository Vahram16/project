<?php

namespace App\Http\Controllers\Api;

use App\Http\Resources\User as UserResource;
use App\UserToken;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;


class AuthController extends Controller
{
    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function login(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|email',
            'password' => 'required',
        ]);

        if ($validator->fails()) {
            $errors = $validator->errors();
            return response()->json([
                'success' => false,
                'errors' => $errors
            ], 400);
        }

        if (Auth::attempt(['email' => $request->email, 'password' => $request->password])) {

            $user = User::where('email', $request->email)->first();

            $userToken = UserToken::where(['ip' => request()->ip(),
                'user_id' => $user->id])->first();

            if (!$userToken){
                $token = md5(openssl_random_pseudo_bytes(32));

                UserToken::create([
                    'api_token' => $token,
                    'user_id' => $user->id,
                    'ip' => request()->ip()
                ]);
            }

            $payload = new UserResource($user);

            return response()->json([
                'success' => true,
                'payload' => $payload
            ], 200);

        } else {
            return response()->json([
                'success' => false,
                'errors' => 'Email or Password is incorrect'
            ]);
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout(Request $request)
    {
        Auth::logout();
        return response()->json([
            'success' => true
        ], 200);
    }

    public function register(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'required|email|unique:users,email',
            'password' => 'required',
        ]);

        if ($validator->fails()) {
            $errors = $validator->errors();
            return response()->json([
                'success' => false,
                'errors' => $errors
            ], 400);
        }

        $user = User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => Hash::make($request->password),
            'role' => 1
        ]);

        $token = md5(openssl_random_pseudo_bytes(32));

        UserToken::create([
            'user_id' => $user->id,
            'ip' => request()->ip(),
            'api_token' => $token
        ]);

        $payload = new UserResource($user);

        return response()->json([
            'payload' => $payload,
            'success' => true
        ], 200);
    }
}
