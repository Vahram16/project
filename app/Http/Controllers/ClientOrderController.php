<?php

namespace App\Http\Controllers;

use App\OrderBook;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cookie;
use App\Book;
use  App\User;
use  App\Order;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class ClientOrderController extends Controller
{


    public function orderProcess(Request $request)
    {

        if (Auth::user()) {
//            $cookie = Cookie::get('books');
//            $booksArray = json_decode($cookie, true);
//            $bookIds = array_keys($booksArray);
//             Book::find($bookIds);
            return redirect(route('createClientOrder'));
        }
        return redirect("/login?page=order");

    }

    public function createClientOrder()
    {
        $cookie = Cookie::get('books');
        $booksArray = json_decode($cookie, true);
        $bookIds = array_keys($booksArray);
        $books = Book::find($bookIds);
        return view('main.createClientOrder', [
            'books' => $books

        ]);


    }

    public function storeClientOrder(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'customer_address' => 'required',
            'customer_phone' => 'required|integer'

        ]);
        if ($validator->fails()) {
            return redirect(route('createClientOrder'))
                ->withErrors($validator)
                ->withInput();
        }

        $cookie = Cookie::get('books');
        $booksArray = json_decode($cookie, true);
        $bookIds = array_keys($booksArray);


        $books = Book::find($bookIds);
        $price = 0;
        foreach ($books as $book) {
            $price += $booksArray[$book->id]['count'] * $book->price;
        }
        $user = Auth::user()->id;
        $order = Order::create([
            'customer_id' => $user,
            'customer_phone' => $request->customer_phone,
            'customer_address' => $request->customer_address,
            'name' => $request->name,
            'price' => $price,
            'status' => 1

        ]);


        foreach ($bookIds as $bookId) {
            OrderBook::create([

                'book_id' => $bookId,
                'order_id' => $order->id,
                'count' => $booksArray[$bookId]['count']


            ]);

        }
//        $orderBooks = OrderBook::find();
//        dd($orderBooks);

        return redirect (route('successOrder',[
            'orderId' => $order->id,
            'booksArray' => $booksArray
        ]))->withCookie(Cookie::forget('books'));

    }

    public function successOrder(Request $request)
    {
        $booksArray = $request -> booksArray;

        $order = Order::find($request -> orderId);

        return view('order.success', [

            'order' => $order,
            'booksArray' => $booksArray

        ]);


    }


}


