<?php

namespace App\Http\Controllers;

use App\OrderBook;
use Illuminate\Http\Request;
use App\Order;
use App\User;
use App\Book;
use Illuminate\Support\Facades\Hash;


class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $orders = Order::paginate(4);

        return view('order.index', [
            'orders' => $orders]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $books = Book::get();

        return view('order.create',
            ['books' => $books]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = User::where([
            [
                'email', $request->email
            ],
            [
                'name', $request->name]
        ])->first();

        if (is_null($user)) {
            $user = User::create([
                'name' => $request->name,
                'email' => $request->email,
                'password' => Hash::make(str_random())
            ]);
        }
        $books = Book::find($request->books);
        $price = 0;
        foreach ($books as $book) {
            $price += $book->price;
        }

        $order = Order::create(array_merge(['customer_id' => $user->id, 'status' => 1, 'price' => $price], $request->all()));

        foreach ($request->books as $book_id) {
            OrderBook::create([
                'book_id' => $book_id,
                'order_id' => $order->id,
            ]);
        }

        return redirect(route('indexOrder'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $books = Book::get();
        $order = Order::find($id);

        return view('order.edit', [
                'order' => $order,
                'books' => $books
            ]
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $order = Order::find($id);
        $order->user->name = $request->name;
        $order->user->email = $request->email;
        $order->customer_address = $request->customer_address;
        $order->customer_phone = $request->customer_phone;

        foreach ($request->books as $book_id) {
            OrderBook::create([
                'book_id' => $book_id,
                'order_id' => $order->id,
            ]);
        }

        $order->save();

        return redirect(route('indexOrder'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $order = Order::find($id);

        foreach ($order->books as $book){
            $book->pivot->delete();
        }

        $order->delete();

        return redirect(route('indexOrder'));
    }

    public function search(Request $request)
    {
        $value = trim($request->value);

        $orders = Order::join('users', 'users.id', '=', 'orders.customer_id')
            ->select('orders.*', 'users.name', 'users.email')
            ->where('orders.customer_phone', 'Like', '%' . $value . '%')
            ->orWhere('orders.customer_address', 'Like', '%' . $value . '%')
            ->orWhere('users.name', 'Like', '%' . $value . '%')
            ->orWhere('users.email', 'Like', '%' . $value . '%')
            ->with('books')
            ->get();

        return response([
            'orders' => $orders
        ]);
    }

    public function bookDestroy(Request $request)
    {
        $book_id = $request->id;
        $orderBook = OrderBook::find($book_id);

        $orderBook->delete();

        return response([
            'success' => true
        ]);
    }

    public function changeStatus(Request $request){
        $orderId=$request -> id;
        $value = $request -> value;

        $order=Order::find($orderId);

        $order -> status = $value;
        $order -> save();

        return response([
            'success' => true
        ]);
    }
}
