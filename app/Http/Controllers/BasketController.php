<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cookie;
use App\Book;
use Illuminate\Support\Facades\Auth;

class BasketController extends Controller
{
    public function index()
    {
        $arrBasketCookie = Cookie::get('books');
        $arrBasket = json_decode($arrBasketCookie, true) ?? [];
        $bookIds = array_keys($arrBasket);
        $books = Book::find($bookIds);

        return view('main.basket.index', [
            'books' => $books,
            'arrBasket' => $arrBasket
        ]);
    }

    /**
     * Method to add a book to basket :)
     *
     * @param Request $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function addBookToBasket(Request $request)
    {
        $bookId = $request->id;
        $book = Book::where('id', $bookId)->first();

        if (empty($book)) {

            return response([
                'success' => false
            ]);

        }

        $arrBasketCookie = Cookie::get('books');
        $arrBasket = json_decode($arrBasketCookie, true);

        if (empty($arrBasket[$bookId])) {
            $arrBasket[$bookId] = [
                'id' => $bookId,
                'count' => 1
            ];
        } else {
            $arrBasket[$bookId]['count'] = $arrBasket[$bookId]['count'] + 1;
        };

        Cookie::queue('books', json_encode($arrBasket), 30);
        return response([
            'success' => true,
            'basketBooksCount' => count($arrBasket)
        ]);

    }

    /**
     * Method to add book count in basket
     *
     * @param Request $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function addBookCountInBasket(Request $request)
    {
        $bookId = $request->id;

        $arrBasketCookie = Cookie::get('books');
        $arrBasket = json_decode($arrBasketCookie, true);
        $arrBasket[$bookId]['count'] = $arrBasket[$bookId]['count'] + 1;

//        Cookie::queue(Cookie::make('books', json_encode($arrBasket), 10000));
        Cookie::queue('books', json_encode($arrBasket), 30);

        return response([
            'success' => true,
            'updatedBookCountInBasket' => $arrBasket[$bookId]['count']
        ]);
    }

    public function minusBookCountInBasket(Request $request)
    {


        $bookId = $request->id;
        $arrBasketCookie = Cookie::get('books');
        $arrBasket = json_decode($arrBasketCookie, true);
        $arrBasket[$bookId]['count'] = $arrBasket[$bookId]['count'] - 1;
        Cookie::queue(Cookie::make('books', json_encode($arrBasket), 10000));
//        $newCookie = Cookie::get('books');
//        $decodeCookie = json_decode($newCookie, true);
//        $newCount=$decodeCookie[$bookId]['count'];

        return response([
            'success' => true,
            'updatedBookCountInBasket' => $arrBasket[$bookId]['count']
        ]);


    }

    public function deleteBookFromBasket(Request $request)
    {
        $bookId = $request->id;
        $arrBasketCookie = Cookie::get('books');
        $arrBasket = json_decode($arrBasketCookie, true);
        $count = $arrBasket[$bookId]['count'];
        unset($arrBasket[$bookId]);
        Cookie::queue(Cookie::make('books', json_encode($arrBasket), 10000));

        return response([
            'count' => $count,
            'success' => true

        ]);
    }

    public function changeBookCountInBasket(Request $request)
    {
        $updatedBookCount = $request->newBookCount;
        $updatedBookCount = floor($updatedBookCount);
        $bookId = $request->id;
        $book = Book::find($bookId);
        $bookPrice = $book->price;
        $arrBasketCookie = Cookie::get('books');
        $arrBasket = json_decode($arrBasketCookie, true);
        $lastCount = $arrBasket[$bookId]['count'];
        $count = $arrBasket[$bookId]['count'] = $arrBasket[$bookId]['count'] - $arrBasket[$bookId]['count'] + $updatedBookCount;
        Cookie::queue('books', json_encode($arrBasket), 10000);
        $bookNewPrice = $count * $bookPrice;


        return response([
                'bookPrice' => $bookPrice,
                'lastCount' => $lastCount,
                'bookNewPrice' => $bookNewPrice,
                'success' => true
            ]

        );


    }

}

