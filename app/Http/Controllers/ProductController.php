<?php

namespace App\Http\Controllers;

use App\ProductImage;
use Illuminate\Http\Request;
use App\Book;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = Book::paginate(3);

        return view('product.index')->with('products', $products);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('product.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'name' => 'required|max:25',
            'author_name' => 'required',
            'description' => 'required',
            'images.*' => 'required',
            'price' => 'required'
        ]);

        if ($validator->fails()) {
            return redirect()->route(route('productCreate'))->withErrors($validator);
        }

        $book = Book::create($request->all());

        if ($request->hasFile('images')) {

            foreach ($request->images as $file) {

                $file->store('products');

                $fileName = $file->hashName();

//                $book->images()->create(['image_name' => $fileName]);

                ProductImage::create([
                    'book_id' => $book->id,
                    'image_name' => $fileName
                ]);
            }
        }
        else{
            ProductImage::create([
                'book_id' => $book->id,
                'image_name' => 'no_image.jpg'
            ]);

        }

        return redirect(route('indexProduct'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $product = Book::find($id);

        return view('product.edit',
            ['product' => $product]
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'max:25',
            'author_name' => 'max:25',
            'description' => 'max:250',
            'price' => 'integer|min:1',
        ]);

        if ($validator->fails()) {
            return redirect(route('editProduct', ['id' => $request->id]))->withErrors($validator)->withInput();
        }

        $book = Book::find($id);

        $book->name = $request->name;
        $book->author_name = $request->name;
        $book->description = $request->description;
        $book->price = $request->price;

        $book->save();

        if ($request->hasFile('images')) {

            foreach ($request->images as $file) {

                $file->store('products');

                $fileName = $file->hashName();

                $book->images()->create(['image_name' => $fileName]);
            }
        }

        return redirect(route('indexProduct'));
    }

    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @throws \Exception
     */
    public function destroy($id)
    {
        $book = Book::find($id);

        foreach ($book->images as $book_image) {
            $book_image->delete();
            unlink(public_path("images/products/{$book_image->image_name}"));
        }

        $book->delete();

        return redirect(route('indexProduct'));
    }

    public function search(Request $request)
    {
        $text = trim($request->text);

        $books = Book::where('name', 'Like', '%' . $text . '%')
            ->orWhere('author_name', 'Like', '%' . $text . '%')
            ->orWhere('description', 'Like', '%' . $text . '%')
            ->with('images')
            ->get();

        return response([
            'books' => $books
        ]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     * @throws \Exception
     */
    public function destroyImage(Request $request)
    {
        $id = $request->id;

        $productImage = ProductImage::find($id);


        unlink(public_path("images/products/{$productImage->image_name}"));
        $productImage->delete();

        return response([
            'success' => true
        ]);
    }
}
