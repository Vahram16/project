<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use App\User;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;

class LoginController extends Controller
{
    public function index()
    {

        return view('login');
    }

    public function postLogin(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'email' => 'required|email|exists:users',
            'password' => 'required'

        ]);

        if ($request->order) {
            if ($validator->fails()) {
                return redirect('/login?page=order')
                    ->withErrors($validator)
                    ->withInput();
            }

        }

        if ($validator->fails()) {
            return redirect('/login')
                ->withErrors($validator)
                ->withInput();
        }


        $user = User::where('email', $request->get('email'))->first();


        if (!Hash::check($request->get('password'), $user->password)) {

            return redirect('/login')
                ->withErrors('your password is incorrect,please try again');
        }

        if ($user->role != User::ADMIN) {
            return redirect('/login');
        }

        $remember = $request->remember == 'on' ? true : false;
        if (isset($request->order)) {

            Auth::attempt(['email' => $request->email, 'password' => $request->password], $remember);
            return redirect(route('createClientOrder'));

        }

        if (Auth::attempt(['email' => $request->email, 'password' => $request->password], $remember)) {
            return redirect('/dashboard');
        } else {
            return redirect('/login');
        }

    }


    public function logout()
    {
        Auth::logout();
        return redirect('/login');

    }
}
