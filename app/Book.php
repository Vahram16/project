<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Book extends Model
{
    protected $fillable = ['name', 'author_name', 'description', 'price', 'average_rating'];
    public function images()
    {
        return $this->hasMany(ProductImage::class);
    }

    public function bookRatings()
    {
        return $this->hasMany(BookRating::class);
    }

    public function bookLast3Ratings()
    {
        return $this->hasMany(BookRating::class)->orderBy('created_at', 'DESC')->limit(3);
    }
}
