<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Article;

class User extends Authenticatable
{
    use Notifiable;

    const ADMIN = 0;
    const USER = 1;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function articles()
    {
        return $this->hasMany(Article::class);
    }
    public function tokens(){

        return $this->hasMany(UserToken::class);

    }
    public function bookRatings(){

        return $this->hasMany(BookRating::class);

    }
    public function orders(){

        return $this->hasMany(Order::class,'customer_id','id');

    }
}