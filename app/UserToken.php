<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\User;

class UserToken extends Model
{
    protected $fillable = [
        'api_token', 'user_id', 'ip',
    ];

    public  function user(){

        return $this->belongsTo(User::class);


    }
}
