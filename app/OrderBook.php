<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderBook extends Model
{
    protected $table = 'order_books';

    protected $fillable = ['book_id', 'order_id', 'count'];

    public function book()
    {
        return $this->belongsTo(Book::class,'book_id','id');
    }
}