<?php

namespace App\Console\Commands;

use App\UserToken;
use Illuminate\Console\Command;
use Carbon\Carbon;

class Login extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'check_users_token';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        $usersToken = UserToken::where('created_at', '<', Carbon::now()->subDays(1))
            ->get();
        $a = Carbon::now();
        dd($a->day);

        foreach ($usersToken as $userToken) {

            $userToken->delete();
        }

        var_dump($usersToken);
    }
}
