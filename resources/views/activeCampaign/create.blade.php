@extends('layouts.client')

@section('content')




    <form action="{{route('storeContact')}}" method="POST">
        <div class="form-group">
            <label for="exampleInputEmail1">Email</label>
            <input type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp"
                   name="email" placeholder="Email">
            <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
        </div>
        {{ csrf_field() }}
        <div class="form-group">
            <label for="exampleInputPassword1">First Name</label>
            <input type="text" class="form-control" id="exampleInputPassword1" name="firstName" placeholder="first name">
        </div>
        <div class="form-group">
            <label for="formGroupExampleInput2">Last Name</label>
            <input type="text" class="form-control" id="formGroupExampleInput2" name="lastName"
                   placeholder="last name">
        </div>

        <div class="form-group">
            <label for="exampleInputPassword1">Phone</label>
            <input type="number" class="form-control" id="exampleInputPassword1" name="phone" placeholder="phone">
        </div>


        <button type="submit" class="btn btn-primary">Submit</button>
    </form>
@endsection