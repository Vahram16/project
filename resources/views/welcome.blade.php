@extends('layouts.client')
@section('content')
    <a href="{{route('basketIndex')}}" style="font-size: 60px;color: darkblue; float:right;margin-top: 20px"><span class="orders"> <i
                    class="fas fa-cart-arrow-down"
                    style="font-size: 60px;color: darkblue; float:right;margin-top: 20px"></i><b></b></span>
    <div class="sum">{{ count(json_decode(Cookie::get('books') , true)) }}</div></a>

    <div class="container">
        <h2>Books</h2>
        <div class="service_wrapper">
            <div class="row">

                @foreach($books as $book)
                    <div class="col-lg-4 jumbotron">
                        <div class="service_block" style="margin-bottom: 60px">
                            @if(count($book->images))

                                <div class="delay-03s animated wow  zoomIn">
                                    <img src="/images/products/{{$book->images[0]->image_name}}" width="150"
                                         height="150">
                                </div>
                            @else <h5>No Images</h5>
                            @endif

                            <h3 class="animated fadeInUp wow">{{$book->name}}</h3>
                            <h4 class="animated fadeInUp wow price">{{$book->price.'$'}}</h4>

                            <p class="animated fadeInDown wow">{!! $book->description !!}
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Animi architecto blanditiis
                                consectetur facilis nam natus, nobis,
                            </p>
                            <button class="add-book-to-basket" data-id="{{$book->id}}"><span> <i class="fas fa-cart-arrow-down" style="font-size: 40px;color: #0f74a8"></i><b></b></span></button>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>

@endsection
@push('scripts')
    <script src="/js/client/index.js"></script>
@endpush