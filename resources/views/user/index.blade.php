@extends('layouts.app')

@section('content')
    <form method="get" action="">

        <div class="input-group stylish-input-group">
            <input type="text" id="txtSearch" name="text" class="form-control" placeholder="Search...">
            <span class="input-group-addon">
                            <button type="submit">
                                <span class="glyphicon glyphicon-search"></span>
                            </button>
                        </span>
        </div>

    </form>

    <table id=table class="table">
        <thead>
        <tr>
            <th scope="col">#</th>
            <th scope="col">Name</th>
            <th scope="col">Email</th>
        </tr>
        </thead>
        <tbody>
        @foreach($users as $one_user)
            <tr>
                <th scope="row">{{$one_user->id}}</th>
                <td>{{$one_user->name}}</td>
                <td>{{$one_user->email}}</td>
                <td><a class="btn btn-warning" href="{{ route('editUser' , ['id' => $one_user->id]) }}">edit </a></td>
                <td><a class="btn btn-danger" href="{{ route('deleteUser' , ['id' => $one_user->id]) }}">delete </a>
                </td>
            </tr>
        @endforeach

        </tbody>
    </table>
    <div style="display: none">
        <table>
            <tr class="tr_example">
                <th scope="row">:id</th>
                <td>:name</td>
                <td>:email</td>
                <td><a class="btn btn-warning" href="{{ route('editUser' , ['id' => ':id']) }}">edit </a></td>
                <td><a class="btn btn-danger" href="{{ route('deleteUser' , ['id' => ':id']) }}">delete </a></td>
            </tr>
        </table>
    </div>
    <a class="btn btn-success" href="{{ route('createUser') }}">Create </a>

    @for($i=1;$i<=ceil(count($allUsers)/10);$i++)
        <a class="btn btn-default" href="{{route('indexUser',['page'=>$i])}}">{{$i}}</a>
    @endfor

@endsection

@push('scripts')
    <script src="/js/user/search.js"></script>
@endpush