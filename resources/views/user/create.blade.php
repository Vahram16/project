@extends('layouts.app')

@section('content')

    <form action="{{ route('storeUser') }}" method="POST" >
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        {{ csrf_field() }}

        <div class="form-group">
            <label for="exampleInputEmail1">Name</label>
            <input name="name"  class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Name">
            <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
        </div>
        <div class="form-group">
            <label for="exampleInputPassword1">Email</label>
            <input  name="email"type="email" class="form-control" id="exampleInputPassword1" placeholder="Email">
        </div>

        <div class="form-group">
            <label for="exampleInputEmail1">Password</label>
            <input name="password" type="password" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Password">
            <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
        </div>

        <button type="submit" class="btn btn-primary">Submit</button>
    </form>

    @endsection