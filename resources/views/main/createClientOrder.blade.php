@extends('layouts.client')

@section('content')


    <div class="container">


        <form action="{{route('storeClientOrder')}}" method="post">
            <div class="form-group">
                <label for="formGroupExampleInput2">Name</label>
                <input name='name' type="text" class="form-control" id="formGroupExampleInput2"
                       placeholder="Another input">
            </div>

            {{ csrf_field() }}

            <div class="form-group">
                <label for="formGroupExampleInput2">Address</label>
                <input name='customer_address' type="text" class="form-control" id="formGroupExampleInput2"
                       placeholder="Another input">
                <div class="form-group">
                    <label for="formGroupExampleInput2">Phone</label>
                    <input name='customer_phone' type="text" class="form-control" id="formGroupExampleInput2"
                           placeholder="Another input">
                </div>




                    @foreach($books as $book)

                        <span class="form-group" style="font-style: normal;">
                           {{$book->name}}
 </span>
                    @endforeach
                <div class="form-group">
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif

                </div>
            </div>
            <button type="submit" class="btn btn-success">Create
            </button>
        </form>


    </div>

    </div>




@endsection