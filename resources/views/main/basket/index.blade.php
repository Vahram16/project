@extends('layouts.client')

@section('content')
    <div class="container">
        <h1>Basket</h1>
        <table class="table table-striped table-bordered " id="table">
            <thead>
            <tr>
                <th>Name</th>
                <th>Author Name</th>
                <th>Price</th>
                <th>Count</th>
                <th>Add Book</th>
                <th>Total Price</th>
                <th>Delete Book</th>
            </tr>

            @php
                $totalPrice = 0
            @endphp

            @foreach($books as $book)
                <tr id="book_{{$book->id}}">
                    @php
                        $bookCountInBasket = $arrBasket[$book->id]['count'];
                        $bookTotalPriceInBasket = $bookCountInBasket * $book->price;
                        $totalPrice = $totalPrice + $bookTotalPriceInBasket;
                        $index = 0;
                    @endphp

                    <td>{{$book->name}}</td>
                    <td>{{$book->author_name}}</td>
                    <td>{{$book->price}}</td>
                    <td ><input class="book-count" type="number"
                                data-id="{{$book->id}}"
                                value="{{ $bookCountInBasket }}"></td>
                    <td>
                        <button class="btn btn-success add-book-count-in-basket"
                                data-id="{{$book->id}}"
                                data-price="{{$book->price}}"
                                data-count="{{ $bookCountInBasket }}">+
                        </button>
                        <button
                                {{$bookCountInBasket==1?"disabled":''}} class="btn btn-danger minus-book-count-in-basket"
                                data-id="{{$book->id}}"
                                data-price="{{$book->price}}"
                                data-count="{{ $bookCountInBasket }}">-
                        </button>
                    </td>
                    <td class="book-price">{{ $bookTotalPriceInBasket }}</td>
                    <td><span class="delete-book" style="float:right">
                        <button class="delete-book-from-basket" type="button"
                                data-id="{{$book->id}}"
                                data-price="{{$book->price}}">
                                <i class="fa fa-times" style="color: red">  </i>
                        </button>
                        </span></td>
                </tr>
            @endforeach
            <p class="totalPrice" style="float: right;margin-bottom: 70px;">
            <span id="totalPrice" data-price="{{ $totalPrice }}"
                  style="font-weight: bold;font-size: 20px;color: #4c110f">
                {{ $totalPrice }}
            </span>
                <i class="fas fa-dollar-sign" style="color: #00FF00"></i>
            </p>
            </tbody>
            </thead>
            <tbody>
            </tbody>
        </table>
        <a href="{{route('orderProcess')}}" class="btn btn-success">Submit </a>
    </div>

@endsection



@push('scripts')
    <script src="/js/client/index.js"></script>
@endpush








