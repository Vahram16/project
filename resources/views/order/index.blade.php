@extends('layouts.app')
@section('content')
    <div class="container">
        <form method="get" action="">

            <div class="input-group stylish-input-group">
                <input type="text" id="txtSearch" name="text" class="form-control" placeholder="Search...">
                <span class="input-group-addon">
                            <button type="submit">
                                <span class="glyphicon glyphicon-search"></span>
                            </button>
            </span>
            </div>

        </form>

        <table class="table" id=table>
            <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">Name</th>
                <th scope="col">Email</th>
                <th scope="col">Address</th>
                <th scope="col">Phone</th>
                <th scope="col">Book</th>

            </tr>
            </thead>
            <tbody>

            @foreach($orders as $order)
                <tr>
                    <th scope="row">{{$order->id}}</th>
                    <td>{{$order->user->name}}</td>
                    <td>{{$order->user->email}}</td>
                    <td>{{$order->customer_address}}</td>
                    <td>{{$order->customer_phone}}</td>
                    <td>
                        <button type="button" class="btn btn-primary" data-toggle="modal"
                                data-target="#myModal_{{$order->id}}">
                            Books
                        </button>
                        @include('order.modal',['books' => $order->books, 'orderId' => $order->id])
                    </td>


                    <td><a class="btn btn-warning" href="{{ route('editOrder' , ['id' => $order->id]) }}">edit</a>
                    </td>
                    <td><a class="btn btn-danger"
                           href="{{ route('deleteOrder' , ['id' => $order->id]) }}">delete </a></td>
                    <td><select class="orderStatus" data-id="{{$order->id}}">
                            <option value="1" {{ $order->status == 1 ? "selected" : ""}}> On its way</option>
                            <option value="2" {{ $order->status == 2 ? "selected" : ""}}> Delivered</option>
                            <option value="3" {{ $order->status == 3 ? "selected" : ""}}> Canceled</option>
                        </select>
                    </td>
                </tr>
            @endforeach

            </tbody>
        </table>

        {{ $orders->links() }}


        <div style="display: none">
            <table>
                <tr class="tr_example">
                    <th scope="row">:id</th>
                    <td>:name</td>
                    <td>:email</td>
                    <td>:customer_address</td>
                    <td>:customer_phone</td>
                    <td>:price</td>
                    <td>
                        <button type="button" class="btn btn-primary" data-toggle="modal"
                                data-target="#myModal_:id">
                            Books
                        </button>
                        @include('order.modal',['books' => [], 'orderId' => ':id'])
                    </td>


                    <td><a class="btn btn-warning" href="{{ route('editOrder' , ['id' => ':id']) }}">edit </a></td>
                    <td><a class="btn btn-danger" href="{{ route('deleteOrder' , ['id' => ':id']) }}">delete </a></td>
                </tr>
            </table>

        </div>
        <a class="btn btn-success" href="">Create </a>

    </div>

@endsection
@push('scripts')
    <script src="/js/order/search.js"></script>
    <script src="/js/order/select.js"></script>

@endpush












