<div class="modal" id="myModal_{{$orderId}}">
    <div class="modal-dialog">
        <div class="modal-content">

            <!-- Modal Header -->
            <div class="modal-header">
                <h4 class="modal-title">Modal Heading</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <!-- Modal body -->
            <div class="modal-body">
                {{--<div id="modalBook_{{$orderId}}">--}}

                <span  style="font-weight: bold">Books Name </span>

                @foreach($books as $book)
                   <div class="myDiv" style="color: #491217;font-style: italic ">{{$book->name.'-'.$book->price.'$'.','}}</div>
                @endforeach
                {{--</div>--}}
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
            </div>

        </div>
    </div>
</div>
