@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="shopping " style="float: right">
            <i class="fas fa-cart-arrow-down" style="font-size: 40px;color: #0f74a8"></i><span><b>Buy</b></span>
            <div class="price"><span style="margin-left: 19px"><b>0 $</b></span></div>
        </div>
        <form action="{{route('storeOrder')}}" method="POST">
            <div class="form-group">
                <label for="formGroupExampleInput2">Name</label>
                <input name='name' type="text" class="form-control" id="formGroupExampleInput2"
                       placeholder="Another input">
            </div>
            {{ csrf_field() }}
            {{--<div class="form-group">--}}
                {{--<label for="formGroupExampleInput">Email</label>--}}
                {{--<input name='email' type="text" class="form-control" id="formGroupExampleInput"--}}
                       {{--placeholder="Example input">--}}
            {{--</div>--}}

            <div class="form-group">
                <label for="formGroupExampleInput2">Address</label>
                <input name='customer_address' type="text" class="form-control" id="formGroupExampleInput2"
                       placeholder="Another input">
                <div class="form-group">
                    <label for="formGroupExampleInput2">Phone</label>
                    <input name='customer_phone' type="text" class="form-control" id="formGroupExampleInput2"
                           placeholder="Another input">
                </div>


                <div class="form-group">
                    <select name ="books[]" id="book" multiple>
                        @foreach($books as $book)
                            <option value="{{ $book->id }}">{{$book->name}}</option>
                        @endforeach
                    </select>

                </div>
            </div>
             <button type="submit" class="btn btn-success">Create
             </button>
        </form>


    </div>

@endsection