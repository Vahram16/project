@extends('layouts.client')

@section('content')

    <div class="container">
        <table class="table">
            <thead>
            <tr>

                <th scope="col">Name</th>
                <th scope="col">Auther_name</th>
                <th scope="col">Count</th>
                <th scope="col">Image</th>
                <th scope="col">Price</th>


            </tr>
            </thead>
            <tbody>
            @foreach($order->books as $book)
                <tr>

                    <td>{{$book->name}}</td>
                    <td>{{$book->author_name}}</td>
                    <td>{{$booksArray[$book->id]['count']}}</td>
                    @if(count($book->images))
                        <td><img src="/images/products/{{$book->images[0]->image_name}}" width="100" height="100"></td>
                    @else
                        <td>NO Images</td>
                    @endif
                    <td>{{$book->price}}</td>
                </tr>
            @endforeach

            </tbody>
        </table>
        <div class="form-group jumbotron">
            ADDRESS: {{$order -> customer_address}} <br>
            PHONE: {{$order -> customer_phone}}<br>
            <a class="btn-success" href="{{route('index')}}"><button class="btn-success">Home</button></a>
        </div>

    </div>











@endsection