@extends('layouts.app')
@section('content')
    <div class="container" xmlns="http://www.w3.org/1999/html">
        <div class="shopping " style="float: right">
            <i class="fas fa-cart-arrow-down" style="font-size: 40px;color: #0f74a8"></i><span><b>Buy</b></span>
            <div class="price"><span style="margin-left: 19px"><b>0 $</b></span></div>
        </div>
        <form action="{{route('updateOrder',['id' => $order->id])}}" method="POST">
            <div class="form-group">
                <label for="formGroupExampleInput2">Name</label>
                <input name='name' type="text" class="form-control" id="formGroupExampleInput2"
                       placeholder="Another input" value="{{$order->user->name}}">
            </div>
            {{ csrf_field() }}
            <div class="form-group">
                <label for="formGroupExampleInput">Email</label>
                <input name='email' type="text" class="form-control" id="formGroupExampleInput"
                       placeholder="Example input" value="{{$order->user->email}}">
            </div>

            <div class="form-group">
                <label for="formGroupExampleInput2">Address</label>
                <input name='customer_address' type="text" class="form-control" id="formGroupExampleInput2"
                       placeholder="Another input" value="{{$order->customer_address}}">
                <div class="form-group">
                    <label for="formGroupExampleInput2">Phone</label>
                    <input name='customer_phone' type="text" class="form-control" id="formGroupExampleInput2"
                           placeholder="Another input" value="{{$order->customer_phone}}">
                </div>

                    <div class="row">
                        <div class="form-group col-sm-6" >
                    <select name="books[]" id="book" multiple>
                        @foreach($books as $book)
                            <option value="{{ $book->id }}">{{$book->name}}</option>

                        @endforeach


                    </select>

                </div>
                        @foreach($order->books as $book)
                            <div class="form-group  ">

                                    <div class="book">
                                        <input type="text" value="{{$book->name}}:{{$book->price}}$" >
                                        <button class="destroy" type="button"  data-id="{{$book->pivot->id}}"> <i class="fas fa-trash"> </i> </button>
                                    </div>
                                @endforeach
                            </div>
                        </div>




            </div>

            <div class="btn">

                <button type="submit" class="btn btn-success">Create
                </button>
            </div>

        </form>


    </div>


@endsection
@push('styles')
    <link rel="stylesheet" href="{{asset('css/style.css')}}">
    @endpush
@push('scripts')

    <script src="/js/order/edit.js"></script>
@endpush