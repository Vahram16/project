@extends('layouts.app')

@section('content')
    <div class="container">

    <h1>Books</h1>
    <form action="{{route('storeProduct')}}" method="POST" enctype="multipart/form-data">
        {{ csrf_field() }}
        <div class="form-group">
            <label for="product">Book name</label>
            <input type="text" id="product" name="name">
        </div>

        <div>
            <label for="product">Author name</label>
            <input type="text" id="product" name="author_name">
        </div>
        <div class="form-group">
            <textarea name="description" id="books_description" name="description"></textarea>
        </div>
        <div class="form-group">
                <div class="form-group">
                    <label for="product">images</label>
                    <input type="file" id="product" name="images[]" multiple>
                </div>
            <div class="form-group">
                <div class="form-group">
                    <label for="product">price</label>
                    <input type="text" id="product" name="price">
                </div>
            </div>
        </div>
        <button>Create</button>
    </form>
    </div>

@endsection

@push('scripts')

<script src="/vendor/unisharp/laravel-ckeditor/ckeditor.js"></script>

<script>
    CKEDITOR.replace( 'books_description' );
</script>

@endpush