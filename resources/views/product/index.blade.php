@extends('layouts.app')

@section('content')
    <form method="get" action="">

        <div class="input-group stylish-input-group">
            <input type="text" id="txtSearch" name="text" class="form-control" placeholder="Search...">
            <span class="input-group-addon">
                            <button type="submit">
                                <span class="glyphicon glyphicon-search"></span>
                            </button>
            </span>
        </div>

    </form>

    <table  class="table" id=table>
        <thead>
        <tr>
            <th scope="col">#</th>
            <th scope="col">Name</th>
            <th scope="col">Auther_name</th>
            <th scope="col">Description</th>
            <th scope="col">Image</th>
            <th scope="col">Price</th>
        </tr>
        </thead>
        <tbody>

        @foreach($products as $product)
            <tr>
                <th scope="row">{{$product->id}}</th>
                <td>{{$product->name}}</td>
                <td>{{$product->author_name}}</td>
                <td>{{$product->description}}</td>
                @if(count($product->images))
                <td><img src="/images/products/{{$product->images[0]->image_name}}" width="100" height="100"></td>
                @else
                    <td>NO Images</td>
                @endif
                <td>{{$product->price}}</td>
                <td><a class="btn btn-warning" href="{{ route('editProduct' , ['id' => $product->id]) }}">edit</a></td>
                <td><a class="btn btn-danger" href="{{route('deleteProduct',['id'=>$product->id])}}">delete </a>
                </td>
            </tr>
        @endforeach

        </tbody>
    </table>
    {{ $products->links() }}

    <div style="display: none">
        <table>
            <tr class="tr_example">
                <th scope="row">:id</th>
                <td>:name</td>
                <td>:author_name</td>
                <td>:description</td>
                <td>:image</td>
                <td>:price</td>


                <td><a class="btn btn-warning" href="{{ route('editProduct' , ['id' => ':id']) }}">edit </a></td>
                <td><a class="btn btn-danger" href="{{ route('deleteProduct' , ['id' => ':id']) }}">delete </a></td>
            </tr>
        </table>
    </div>
    <a class="btn btn-success" href="{{ route('createProduct') }}">Create </a>


@stop
@push('scripts')
<script src="/js/products/product.js"></script>



@endpush

