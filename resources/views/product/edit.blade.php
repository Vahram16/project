@extends('layouts.app')
@section('content')
    <div class="container">
        <h1>Books</h1>
        <form action="{{ route('updateProduct' , ['id' => $product->id]) }}" method="POST"
              enctype="multipart/form-data">
            {{ csrf_field() }}
            <div class="form-group">
                <label for="product">Book name</label>
                <input type="text" name="name" id="product" value="{{$product->name}}">
            </div>

            <div class="form-group">
                <label for="product">Author name</label>
                <input type="text" name="author_name" id="product" value="{{$product->author_name}}">
            </div>
            <div class="form-group">
                <textarea name="description" id="description" class="description"
                          data-book-description="{{$product->description}}"></textarea>
            </div>
            <div class="form-group">
                <label for="product">Image</label>
                @if(count($product->images))
                    <div class="row">


                        @foreach($product->images as $one_image)
                        <div class="form-group div-images">
                            <img src="/images/products/{{$one_image->image_name}}" width="100" height="100">
                            <button class="destroy btn btn-danger" type="button"  data-id="{{$one_image->id}}">
                                delete
                            </button>

                    </div>

                    @endforeach
                    </div>
                @endif
                <input type="file" name="images[]">
            </div>
            <div class="form-group">
                <label for="product">price</label>
                <input type="text" name="price" id="product" value="{{$product->price}}">
            </div>
            <button type="submit" class="btn btn-primary">Submit</button>
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif

        </form>
    </div>











@endsection
@push('scripts')
    <script src="/vendor/unisharp/laravel-ckeditor/ckeditor.js"></script>

    <script>
        CKEDITOR.replace('description');
    </script>
    <script src="/js/products/edit.js"></script>
@endpush